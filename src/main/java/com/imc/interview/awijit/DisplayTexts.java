package com.imc.interview.awijit;

public class DisplayTexts {

    public static final String WELCOME_MESSAGE = "Welcome to Rock, Paper & Scissors!\nHow many games would you " +
            "like to play? Please enter a number:";
    public static final String NOT_INTEGER_ERROR = "Please enter an integer to proceed. Try Again? (yes|no)";
    public static final String MAKE_YOUR_CHOICE = "Make your choice (%s): ";
    public static final String YES_ANSWER_REGEX = "(?i)yes|y";
    public static final String YOUR_CHOICE = "Your choice: ";
    public static final String COMPUTERS_NAME = "Computer";
    public static final String COMPUTERS_CHOICE = "Computer's choice: ";
    public static final String YOU_WON = "Conrgratulations, you won!";
    public static final String YOU_LOST = "Oops, sorry! You lost :( ";
    public static final String MATCH_TIED = "The match has tied. No winner.";
    public static final String FINAL_SCORE = "Final Score!\n%s: %s, %s: %s, Ties: %s";
    public static final String ENTER_NAME = "Enter your name, human: ";

}

package com.imc.interview.awijit.ResultLogic;

public class NormalCalculator implements ConditionCalculator {
    @Override
    public Result result(int player1choice, int player2choice) {
        if ((player1choice + 1) % 3 == player2choice) {
            return Result.LOSE;
        } else if (player1choice == player2choice) {
            return Result.DRAW;
        } else {
            return Result.WIN;
        }
    }
}

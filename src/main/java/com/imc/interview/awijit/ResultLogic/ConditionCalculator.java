package com.imc.interview.awijit.ResultLogic;

public interface ConditionCalculator {
    Result result(int player1choice, int player2choice);
}

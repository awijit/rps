package com.imc.interview.awijit.userinterface;

import com.imc.interview.awijit.DisplayTexts;
import com.imc.interview.awijit.players.Player;

import java.util.Scanner;

public class CommandLineInterface implements UserInterface {

    private int numberOfRounds;
    private Scanner scanner = new Scanner(System.in);

    public int getNumberOfRounds() {
        boolean tryAgain;

        do {
            System.out.println(DisplayTexts.WELCOME_MESSAGE);
            try {
                numberOfRounds = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException ex) {
                System.out.println(DisplayTexts.NOT_INTEGER_ERROR);
                tryAgain = scanner.nextLine().matches(DisplayTexts.YES_ANSWER_REGEX);
            }
        } while (tryAgain);

        return numberOfRounds;
    }

    @Override
    public void displayFinalResult(Player player1, Player player2) {
        System.out.printf(DisplayTexts.FINAL_SCORE,
                player1.getName(), player1.getWins(),
                player2.getName(), player2.getWins(),
                numberOfRounds - player1.getWins() - player2.getWins());
    }
}

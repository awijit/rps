package com.imc.interview.awijit.userinterface;

import com.imc.interview.awijit.players.Player;

public interface UserInterface {

    public int getNumberOfRounds();

    public void displayFinalResult(Player player1, Player player2);

}

package com.imc.interview.awijit.choices;

class Scissors extends Choice {
    private int id;

    Scissors() {
        this.id = Choices.SCISSORS.ordinal();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return Choices.SCISSORS.name();
    }
}

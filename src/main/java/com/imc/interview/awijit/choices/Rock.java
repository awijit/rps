package com.imc.interview.awijit.choices;

// rock has an id of 0 and displays rock
class Rock extends Choice {

    private int id;

    Rock() {
        this.id = Choices.ROCK.ordinal();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return Choices.ROCK.name();
    }
}

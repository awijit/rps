package com.imc.interview.awijit.choices;

import com.imc.interview.awijit.exceptions.InvalidChoiceException;

public interface ChoiceFactory {
    Choice getChoiceByString(String choice) throws InvalidChoiceException;

    Choice getChoiceById(int choice) throws InvalidChoiceException;
}

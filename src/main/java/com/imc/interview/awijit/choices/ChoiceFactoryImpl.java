package com.imc.interview.awijit.choices;

import com.imc.interview.awijit.exceptions.InvalidChoiceException;

public class ChoiceFactoryImpl implements ChoiceFactory {


    public Choice getChoiceByString(String choice) throws InvalidChoiceException {
        try {
            return getChoiceByEnum(Choices.valueOf(choice.toUpperCase()));
        } catch (IllegalArgumentException ex) {
            throw new InvalidChoiceException(choice);
        }
    }

    public Choice getChoiceById(int choice) throws InvalidChoiceException {
        return getChoiceByEnum(Choices.values()[choice]);
    }

    private Choice getChoiceByEnum(Choices choice) throws InvalidChoiceException {
        switch (choice) {
            case ROCK: return new Rock();
            case PAPER: return new Paper();
            case SCISSORS: return new Scissors();
            default: throw new InvalidChoiceException();
        }
    }

}

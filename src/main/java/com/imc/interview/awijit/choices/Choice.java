package com.imc.interview.awijit.choices;

public abstract class Choice {

    public abstract int getId();

    @Override
    public abstract String toString();

}

package com.imc.interview.awijit.choices;

class Paper extends Choice {

    private int id;

    Paper() {
        this.id = Choices.PAPER.ordinal();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return Choices.PAPER.name();
    }
}

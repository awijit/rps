package com.imc.interview.awijit;

import com.imc.interview.awijit.ResultLogic.ConditionCalculator;
import com.imc.interview.awijit.ResultLogic.NormalCalculator;
import com.imc.interview.awijit.ResultLogic.Result;
import com.imc.interview.awijit.choices.Choice;
import com.imc.interview.awijit.players.Player;

class Round {

    private ConditionCalculator calculator = new NormalCalculator();

    void play(Player human, Player computer) {
        Choice humanChoice = human.getChoice();
        Choice computerChoice = computer.getChoice();

        System.out.println(DisplayTexts.YOUR_CHOICE + humanChoice.toString());
        System.out.println(DisplayTexts.COMPUTERS_CHOICE + computerChoice.toString());

        Result result = calculator.result(humanChoice.getId(), computerChoice.getId());

        switch (result)
        {
            case WIN: human.addWin();
                System.out.println(DisplayTexts.YOU_WON);
            break;
            case LOSE: computer.addWin();
                System.out.println(DisplayTexts.YOU_LOST);
            break;
            default:
                System.out.println(DisplayTexts.MATCH_TIED);
        }
    }
}

package com.imc.interview.awijit.players;

import com.imc.interview.awijit.DisplayTexts;
import com.imc.interview.awijit.choices.Choice;
import com.imc.interview.awijit.choices.ChoiceFactory;
import com.imc.interview.awijit.choices.ChoiceFactoryImpl;
import com.imc.interview.awijit.choices.Choices;
import com.imc.interview.awijit.exceptions.InvalidChoiceException;

import java.util.Arrays;
import java.util.Scanner;

public class Human extends Player {

    private ChoiceFactory choiceFactory = new ChoiceFactoryImpl();
    String name;
    private Scanner scanner = new Scanner(System.in);

    public Human() {
        System.out.print(DisplayTexts.ENTER_NAME);
        name = scanner.nextLine();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Choice getChoice() {
        System.out.printf(DisplayTexts.MAKE_YOUR_CHOICE, Arrays.asList(Choices.values()));
        String playerChoiceString = scanner.nextLine();
        try {
            return choiceFactory.getChoiceByString(playerChoiceString);
        } catch (InvalidChoiceException ex) {
            System.out.println(ex.getErrorMessage());
            throw new RuntimeException();
        }
    }
}

package com.imc.interview.awijit.players;

import com.imc.interview.awijit.choices.Choice;

public abstract class Player {

    private int wins = 0;

    public void addWin() {
        wins++;
    }

    public int getWins() {
        return wins;
    }

    public abstract Choice getChoice();

    public abstract String getName();

}

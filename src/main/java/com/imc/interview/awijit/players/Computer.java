package com.imc.interview.awijit.players;

import com.imc.interview.awijit.DisplayTexts;
import com.imc.interview.awijit.choices.Choice;
import com.imc.interview.awijit.choices.ChoiceFactory;
import com.imc.interview.awijit.choices.ChoiceFactoryImpl;
import com.imc.interview.awijit.choices.Choices;
import com.imc.interview.awijit.exceptions.InvalidChoiceException;

public class Computer extends Player {

    private ChoiceFactory choiceFactory = new ChoiceFactoryImpl();

    private int numOfTokens = Choices.values().length;

    @Override
    public String getName() {
        return DisplayTexts.COMPUTERS_NAME;
    }

    @Override
    public Choice getChoice() {
        int i = (int) (Math.random() * numOfTokens);
        try {
            return choiceFactory.getChoiceById(i);
        } catch (InvalidChoiceException ex) {
            System.out.println(ex.getErrorMessage());
            throw new RuntimeException();
        }
    }
}

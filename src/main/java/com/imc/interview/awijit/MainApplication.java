package com.imc.interview.awijit;

import com.imc.interview.awijit.players.Computer;
import com.imc.interview.awijit.players.Human;
import com.imc.interview.awijit.players.Player;
import com.imc.interview.awijit.userinterface.CommandLineInterface;
import com.imc.interview.awijit.userinterface.UserInterface;

public class MainApplication {
    public static void main(String args[]) {

        UserInterface ui = new CommandLineInterface();

        Round round = new Round();
        Player player1 = new Human();
        Player player2 = new Computer();

        int numberOfRounds = ui.getNumberOfRounds();

        for(int i = 0; i < numberOfRounds; i++) {
            round.play(player1, player2);
        }

        ui.displayFinalResult(human, computer);


    }
}


 
 
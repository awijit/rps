package com.imc.interview.awijit.exceptions;

public class InvalidChoiceException extends Exception {
    private String attemptedToken;

    public InvalidChoiceException(String attemptedToken) {
        this.attemptedToken = attemptedToken;
    }

    public InvalidChoiceException() {}

    public String getErrorMessage() {
        return String.format("Invalid token '%s'", attemptedToken);
    }
}
